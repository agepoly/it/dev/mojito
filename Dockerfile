FROM debian:bullseye AS runtime
RUN apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates

FROM rust:latest AS rust_build
RUN cargo install cargo-build-deps
RUN cargo new app
WORKDIR /app
COPY Cargo.toml Cargo.lock ./
RUN cargo build-deps --release
COPY migrations ./migrations
COPY sqlx-data.json ./sqlx-data.json
COPY src ./src
RUN cargo build --release --bin mojito

FROM runtime
WORKDIR /app
COPY migrations ./migrations
COPY Rocket.toml ./Rocket.toml
COPY --from=rust_build /app/target/release/mojito ./mojito
CMD ["./mojito"]
