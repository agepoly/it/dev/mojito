use rocket::serde::Serialize;
use schemars::JsonSchema;
use sqlx::Error;
use std::borrow::Borrow;

#[derive(JsonSchema, Serialize, Debug)]
pub enum SimpleDbError {
    RowNotFound,
    NotNullViolation,
    ForeignKeyViolation,
    UniqueViolation,
    ConnectionError,
    UnknownError,
}

impl From<Error> for SimpleDbError {
    fn from(e: Error) -> Self {
        match e {
            Error::RowNotFound => SimpleDbError::RowNotFound,
            Error::PoolClosed | Error::PoolTimedOut => SimpleDbError::ConnectionError,
            Error::Database(dbe) => {
                if let Some(code) = dbe.code() {
                    eprintln!("POSTGRESQL ERROR = {:?}", dbe.code());
                    match code.borrow() {
                        "23502" => SimpleDbError::NotNullViolation,
                        "23503" => SimpleDbError::ForeignKeyViolation,
                        "23505" => SimpleDbError::UniqueViolation,
                        _ => SimpleDbError::UnknownError,
                    }
                } else {
                    SimpleDbError::UnknownError
                }
            }
            _ => SimpleDbError::UnknownError,
        }
    }
}

