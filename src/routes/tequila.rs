
use rocket::serde::json::Json;
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Serialize, Deserialize};
use sqlx::PgPool;
use uuid::Uuid;

use crate::models::service::Service;
use crate::models::messages::GenericError;
use crate::tequila::{TequilaError, TequilaAttributes};
use crate::utils::SimpleDbError;

#[derive(JsonSchema, Deserialize)]
pub struct PostTequilaRequestForm {
    pub id: i32,
    pub secret_token: Uuid,
}

#[derive(JsonSchema, Serialize)]
pub struct PostTequilaRequestResponse {
    pub key: String,
    pub url: String,
}

#[openapi(tag = "Tequila")]
#[post("/tequila/request", data = "<data>")]
pub async fn post_tequila_request(data: Json<PostTequilaRequestForm>, pool: &rocket::State<PgPool>) -> Result<Json<PostTequilaRequestResponse>, GenericError> {
    let service = sqlx::query_as!(
        Service,
        r#"
            SELECT * FROM services
            WHERE id = $1 AND secret_token = $2 AND activated = true
        "#,
        &data.id,
        &data.secret_token,
    )
    .fetch_one(pool.inner())
    .await
    .map_err(|e| match SimpleDbError::from(e) {
        SimpleDbError::RowNotFound => GenericError::not_found(),
        err => {
            eprintln!("Database Error : {:?}", err);
            GenericError::internal()
        }
    })?;

    println!("Tequila Create Request for {} : requesting {}", &service.name, &service.tequila_attributes_str());

    let tequila_key = crate::tequila::Tequila::from_service(service).create_request().await.map_err(|err| {
        eprintln!("Tequila Error : {:?}", err);
        GenericError::internal()
    })?;

    Ok(Json(PostTequilaRequestResponse{
        key: tequila_key.clone(),
        url: format!("{}{}", crate::tequila::TEQUILA_CLIENT_AUTH_URL, tequila_key)
    }))
}

#[derive(JsonSchema, Deserialize)]
pub struct PostTequilaFetchAttributesForm {
    pub id: i32,
    pub secret_token: Uuid,
    pub key: String,
    pub auth_check: String
}

#[openapi(tag = "Tequila")]
#[post("/tequila/fetch-attributes", data = "<data>")]
pub async fn post_tequila_fetch_attributes(data: Json<PostTequilaFetchAttributesForm>, pool: &rocket::State<PgPool>) -> Result<Json<TequilaAttributes>, GenericError> {
    let service = sqlx::query_as!(
        Service,
        r#"
            SELECT * FROM services
            WHERE id = $1 AND secret_token = $2 AND activated = true
        "#,
        &data.id,
        &data.secret_token,
    )
    .fetch_one(pool.inner())
    .await
    .map_err(|e| match SimpleDbError::from(e) {
        SimpleDbError::RowNotFound => GenericError::not_found(),
        err => {
            eprintln!("Database Error : {:?}", err);
            GenericError::internal()
        }
    })?;

    let attributes = crate::tequila::Tequila::from_service(service).fetch_attributes(&data.key, &data.auth_check).await.map_err(|err| match err {
        TequilaError::BadSessionKey => GenericError::bad_format("bad session key"),
        err => {
            eprintln!("Tequila Error : {:?}", err);
            GenericError::internal()
        }
    })?;

    Ok(Json(attributes))
}
