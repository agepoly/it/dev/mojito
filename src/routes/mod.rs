use rocket::serde::json::Json;
use rocket_okapi::openapi;

use crate::models::service::ServiceAttribute;
use crate::models::messages::SimpleMessage;

pub mod services;
pub mod tequila;

#[openapi(tag = "Misc")]
#[get("/health-check")]
pub fn health_check() -> Json<SimpleMessage> {
    SimpleMessage::json_ok("Health check OK !")
}

#[openapi(tag = "Misc")]
#[get("/service-attributes")]
pub fn get_attributes() -> Json<Vec<ServiceAttribute>> {
    Json(ServiceAttribute::all_values())
}