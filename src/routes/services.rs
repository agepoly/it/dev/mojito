use rocket::serde::json::Json;
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize};
use sqlx::PgPool;

use crate::models::service::{Service, ServiceAttribute};
use crate::models::messages::GenericError;
use crate::utils::SimpleDbError;

#[derive(JsonSchema, Deserialize)]
pub struct PostServiceForm {
    name: String,
    return_url: String,
    service_attributes: Vec<ServiceAttribute>,
    admin_token: String,
}

#[openapi(tag = "Services")]
#[post("/services", data = "<data>")]
pub async fn post_groups(data: Json<PostServiceForm>, pool: &rocket::State<PgPool>) -> Result<Json<Service>, GenericError> {
    if data.admin_token != crate::env::ADMIN_BEARER.to_string() {
        return Err(GenericError::forbidden())
    }
    
    let attributes: Vec<String> = data.service_attributes.iter().map(|s| s.tequila_attribute().to_string()).collect();
    let attributes = attributes.join(",");
    match sqlx::query_as!(
        Service,
        r#"
            INSERT INTO services (name, tequila_attributes, return_url)
            VALUES($1, $2, $3)
            RETURNING *
        "#,
        &data.name,
        attributes,
        &data.return_url
    )
    .fetch_one(pool.inner())
    .await
    {
        Ok(group) => Ok(Json(group)),
        Err(err) => match SimpleDbError::from(err) {
            SimpleDbError::UniqueViolation => Err(GenericError::bad_format("conflict")),
            _ => Err(GenericError::internal()),
        },
    }
}

#[openapi(tag = "Services")]
#[get("/services?<admin_token>")]
pub async fn get_groups(admin_token: String, pool: &rocket::State<PgPool>) -> Result<Json<Vec<Service>>, GenericError> {
    if admin_token != crate::env::ADMIN_BEARER.to_string() {
        return Err(GenericError::forbidden())
    }
    
    let services = sqlx::query_as!(
        Service,
        r#"
            SELECT * FROM services
        "#
    )
    .fetch_all(pool.inner())
    .await.map_err(|_| GenericError::internal())?;
    Ok(Json(services))
}
