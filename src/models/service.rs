use schemars::JsonSchema;
use rocket::serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, JsonSchema, Debug)]
pub struct Service {
    pub id: i32,
    pub name: String,
    pub secret_token: Uuid,
    pub activated: bool,
    pub tequila_attributes: String,
    pub return_url: String
}

impl Service {
    // pub fn service_attributes(&self) -> Vec<ServiceAttribute> {
    //     let attr_str: Vec<&str> = self.tequila_attributes.split(",").into_iter().collect();
    //     let mut attributes: Vec<ServiceAttribute> = Vec::default();
    //     for a in attr_str.into_iter() {
    //         if let Some(attribute) = ServiceAttribute::from_tequila(a) {
    //             attributes.push(attribute);
    //         }
    //     }
    //     attributes
    // }
    
    pub fn tequila_attributes(&self) -> Vec<String> {
        self.tequila_attributes.split(',').map(|s| s.to_string()).collect()
    }

    pub fn tequila_attributes_str(&self) -> String {
        self.tequila_attributes.to_string()
    }
}

#[derive(Serialize, Deserialize, JsonSchema, Clone, Debug)]
#[serde(rename_all = "snake_case")]
pub enum ServiceAttribute {
    Sciper,
    EpflEmail,
    GasparUsername,
    Name,
    Firstname,
    DisplayName,
    Groups,
    Units
}

impl ServiceAttribute {
    pub fn all_values() -> Vec<ServiceAttribute> {
        vec!(
            ServiceAttribute::Sciper,
            ServiceAttribute::EpflEmail,
            ServiceAttribute::GasparUsername,
            ServiceAttribute::Name,
            ServiceAttribute::Firstname,
            ServiceAttribute::DisplayName,
            ServiceAttribute::Groups,
            ServiceAttribute::Units,
        )
    }

    pub fn tequila_attribute(&self) -> &'static str {
        match &self {
            ServiceAttribute::Sciper => "uniqueid",
            ServiceAttribute::EpflEmail => "email",
            ServiceAttribute::GasparUsername => "username",
            ServiceAttribute::Name => "name",
            ServiceAttribute::Firstname => "firstname",
            ServiceAttribute::DisplayName => "displayname",
            ServiceAttribute::Groups => "group",
            ServiceAttribute::Units => "allunits",
        }
    }

    // pub fn from_tequila<S: AsRef<str>>(tequila_attribute: S) -> Option<ServiceAttribute> {
    //     match tequila_attribute.as_ref() {
    //         "uniqueid" => Some(ServiceAttribute::Sciper),
    //         "email" => Some(ServiceAttribute::EpflEmail),
    //         "username" => Some(ServiceAttribute::GasparUsername),
    //         "name" => Some(ServiceAttribute::Name),
    //         "firstname" => Some(ServiceAttribute::Firstname),
    //         "displayname" => Some(ServiceAttribute::DisplayName),
    //         "group" => Some(ServiceAttribute::Groups),
    //         "allunits" => Some(ServiceAttribute::Units),
    //         _ => None
    //     }
    // }
}