use okapi::openapi3::Responses;
use rocket_okapi::OpenApiError;
use rocket_okapi::gen::OpenApiGenerator;
use rocket_okapi::response::OpenApiResponderInner;
use schemars::JsonSchema;
use rocket::serde::{json::Json, Serialize};
use rocket::response::Responder;
use rocket::{response, Request, Response};
use serde::Deserialize;
 

#[derive(Serialize, JsonSchema)]
pub struct SimpleMessage {
    pub id: &'static str,
    pub message: String
}

impl SimpleMessage {

    pub fn json_ok<S: AsRef<str>>(message: S) -> Json<Self> {
        SimpleMessage {
            id: "OK",
            message: message.as_ref().to_string()
        }.into()
    }

}

#[derive(Serialize, Deserialize, Debug, JsonSchema)]
pub struct GenericError {
    id: &'static str,
    message: String,

    #[serde(skip)]
    http_code: u16,
}

impl GenericError {
    pub fn bad_format<S: AsRef<str>>(message: S) -> Self {
        GenericError {
            id: "ERR_BAD_FORMAT",
            http_code: 400,
            message: message.as_ref().to_string()
        }
    }

    pub fn not_found() -> Self {
        GenericError {
            id: "ERR_NOT_FOUND",
            http_code: 404,
            message: "Not found".to_string()
        }
    }

    pub fn forbidden() -> Self {
        GenericError {
            id: "ERR_FORBIDDEN",
            http_code: 403,
            message: "Forbidden".to_string()
        }
    }

    pub fn internal() -> Self {
        GenericError {
            id: "ERR_INTERNAL",
            http_code: 500,
            message: "Please contact AGEPoly's IT Manager to fix this problem".to_string()
        }
    }
}

impl<'r> Responder<'r, 'static> for GenericError {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
        let body = serde_json::to_string(&self).unwrap();
        Response::build()
            .sized_body(body.len(), std::io::Cursor::new(body))
            .header(rocket::http::ContentType::JSON)
            .status(rocket::http::Status::new(self.http_code))
            .ok()
    }
}

impl OpenApiResponderInner for GenericError {
    fn responses(_gen: &mut OpenApiGenerator) -> Result<Responses, OpenApiError> {
        use okapi::openapi3::{RefOr, Response};
        // let schema = gen.json_schema::<GenericError>();
        Ok(Responses {
            responses: okapi::map! {
                "400".to_owned() => RefOr::Object(Response {
                    description: "Bad Format".to_owned(),                        
                    ..Default::default()
                }),
                "404".to_owned() => RefOr::Object(Response {
                    description: "Not Found".to_owned(),
                    ..Default::default()
                }),
                "500".to_owned() => RefOr::Object(Response {
                    description: "Internal Server error".to_owned(),
                    ..Default::default()
                }),
            },
            ..Default::default()
        })
    }
}