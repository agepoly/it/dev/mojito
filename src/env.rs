use lazy_static::lazy_static;

lazy_static! {
    pub static ref DATABASE_URL: String = std::env::var("DATABASE_URL").expect("Missing DATABASE_URL env variable");
    pub static ref ADMIN_BEARER: String = std::env::var("ADMIN_BEARER_TOKEN").expect("Missing ADMIN_BEARER_TOKEN env variable");
}

#[allow(unused_must_use)]
#[allow(clippy::clone_on_copy)]
pub fn check() {
    if let Ok(path) = dotenv::dotenv() {
        println!("Loaded .env file (at {:?})", path);
    }

    DATABASE_URL.clone();
    ADMIN_BEARER.clone();
}