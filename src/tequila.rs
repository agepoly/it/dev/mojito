//! Tequila service interface by Marcus Cemes (2022), slightly modified for Agevote
//! Original source code:
//! https://github.com/MarcusCemes/clubhouse-bridge/blob/main/src/tequila.rs
//! 
//! Official (EPFL) Tequila documentation in french:
//! https://www.epfl.ch/campus/services/ressources-informatiques/authentification/tequila/

use std::collections::{BTreeMap, BTreeSet};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use log::error;
use reqwest::{Client, StatusCode};
use std::time::Duration;

use crate::models::service::Service;

const TEQUILA_BASE_URL: &str = "https://tequila.epfl.ch/cgi-bin/tequila";
pub const TEQUILA_CLIENT_AUTH_URL: &str = "https://tequila.epfl.ch/cgi-bin/tequila/auth?requestkey=";

const REQUEST_TIMEOUT_S: u64 = 8;

const LINE_DELIMITER: char = '\n';
const KEY_VALUE_DELIMITER: char = '=';
const VALUE_DELIMITER: char = ',';

type TequilaRawAttributes = Vec<(String, TequilaValue)>;

#[derive(Debug)]
pub struct Tequila {
    client: Client,
    service: Service,
}

impl Tequila {
    pub fn from_service(service: Service) -> Self {
        Self {
            client: Client::new(),
            service,
        }
    }

    pub async fn create_request(&self) -> Result<String, TequilaError> {
        let tequila_attributes_str = self.service.tequila_attributes_str();
        self.make_tequila_call(
            TequilaMethod::CreateRequest,
            &[
                ("urlaccess", &self.service.return_url),
                ("service", &self.service.name),
                ("request", &tequila_attributes_str),
                ("mode_auth_check", "1"),
            ],
        )
        .await
        .and_then(|response| {
            response
            .into_iter()
            // Extract the `key` line from the body
            .find(|(k, _)| k.as_str() == "key")
            .map(|(_, v)| v)
            // Assert that it's a singular value, not an array
            .and_then(|v| match v {
                TequilaValue::Single(key) => Some(key),
                _ => None,
            })
            // Convert Option<T> to Result<T, E>
            .ok_or_else(|| {
                error!("No key in Tequila response");
                TequilaError::ProtocolError
            })
        })
    }

    /// Executes a `fetchattributes` Tequila call, returning the user's
    /// attributes after a successful authentication flow.
    pub async fn fetch_attributes(&self, key: &str, auth_check: &str) -> Result<TequilaAttributes, TequilaError> {
        let attr = self.make_tequila_call(TequilaMethod::FetchAttributes, &[("key", key), ("auth_check", auth_check)]).await?;
        self.parse_raw_attributes(attr)
    }

    fn parse_raw_attributes(&self, raw_attributes: TequilaRawAttributes) -> Result<TequilaAttributes, TequilaError> {
        macro_rules! extract_value {
            ($name:literal, $map:ident, $req:ident, $mis:ident) => {
                if let Some(value) = $map.remove($name) {
                    Some(value.try_into()?)
                } else {
                    if $req.contains($name) {
                        $mis.push($name.into())
                    }
                    None
                }
            };
        }
        
        let mut map: BTreeMap<String, TequilaValue> = raw_attributes.clone().into_iter().collect::<BTreeMap<_, _>>();
        let required_attributes: BTreeSet<String> = BTreeSet::from_iter(self.service.tequila_attributes().into_iter());
        let mut missing_attributes: Vec<String> = vec![];
        let r = TequilaAttributes {
            sciper: extract_value!("uniqueid", map, required_attributes, missing_attributes),
            epfl_email: extract_value!("email", map, required_attributes, missing_attributes),
            gaspar_username: extract_value!("username", map, required_attributes, missing_attributes),
            name: extract_value!("name", map, required_attributes, missing_attributes),
            firstname: extract_value!("firstname", map, required_attributes, missing_attributes),
            display_name: extract_value!("displayname", map, required_attributes, missing_attributes),
            groups: extract_value!("group", map, required_attributes, missing_attributes),
            units: extract_value!("allunits", map, required_attributes, missing_attributes),
        };

        if missing_attributes.is_empty() {
            println!("Tequila attributes for service {} : {:?}", self.service.name, &r);
            Ok(r)
        } else {
            eprintln!("Missing tequila attributes : {:?}, raw_attributes: {:?}", required_attributes, raw_attributes);
            Err(TequilaError::ParserMissingFields(missing_attributes))
        }
    }

    /// Executes a a given Tequila call, returning the response.
    async fn make_tequila_call(&self, method: TequilaMethod, params: &[(&str, &str)]) -> Result<Vec<(String, TequilaValue)>, TequilaError> {
        let url = format!("{}/{}", TEQUILA_BASE_URL, method.as_str());
        let response = self
            .client
            .post(&url)
            .header("Content-Type", "text/plain")
            .body(serialize_params(params))
            .timeout(Duration::from_secs(REQUEST_TIMEOUT_S))
            .send()
            .await
            .map_err(|error| {
                error!("Connection refused: {}", error);
                TequilaError::ConnectionRefused
            })?;

        match response.status() {
            StatusCode::OK => {
                let response_text = response.text().await.map_err(|error| {
                    error!("Expected parameters from Tequila, no response text:\n{}", error);
                    TequilaError::ProtocolError
                })?;

                Ok(deserialize_params(&response_text))
            }
            StatusCode::UNAVAILABLE_FOR_LEGAL_REASONS => Err(TequilaError::BadSessionKey),

            StatusCode::FORBIDDEN => {
                warn!("Tequila refused connection, the server must be on EPFL network!");
                Err(TequilaError::ActionForbidden)
            }

            status => {
                error!("Tequila returned an unknown status code of {}!", status,);
                Err(TequilaError::ProtocolError)
            }
        }
    }
}

/// Serializes a list of key-value pairs delimited by equals signs and
/// new lines, as understood by Tequila.
fn serialize_params(params: &[(&str, &str)]) -> String {
    params
        .iter()
        .map(|(k, v)| format!("{k}{KEY_VALUE_DELIMITER}{v}"))
        .collect::<Vec<_>>()
        .join(LINE_DELIMITER.to_string().as_str())
}

/// Deserializes Tequila's equals and new line delimited payload into
/// a collection of key-value pairs.
fn deserialize_params(params: &str) -> Vec<(String, TequilaValue)> {
    params
        .trim()
        .split(LINE_DELIMITER)
        .filter_map(|line| line.split_once(KEY_VALUE_DELIMITER))
        .map(deserialize_param_value)
        .collect()
}

fn deserialize_param_value((key, value): (&str, &str)) -> (String, TequilaValue) {
    use TequilaValue::*;
    let key = key.to_string();

    let value = match value.contains(VALUE_DELIMITER) {
        true => Multiple(value.split(VALUE_DELIMITER).map(String::from).collect()),
        false => Single(value.to_string()),
    };

    (key, value)
}

#[derive(Debug, Serialize, Deserialize, Clone, JsonSchema)]
pub struct TequilaAttributes {
    pub sciper: Option<String>,
    pub epfl_email: Option<String>,
    pub gaspar_username: Option<String>,
    pub name: Option<String>,
    pub firstname: Option<String>,
    pub display_name: Option<String>,
    pub groups: Option<Vec<String>>,
    pub units: Option<Vec<String>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum TequilaValue {
    Single(String),
    Multiple(Vec<String>),
}

impl TryInto<String> for TequilaValue {
    type Error = TequilaError;
    fn try_into(self) -> Result<String, Self::Error> {
        match self {
            Self::Single(str) => Ok(str),
            Self::Multiple(_) => Err(TequilaError::ParserBadFormat),
        }
    }
}

impl TryInto<Vec<String>> for TequilaValue {
    type Error = TequilaError;
    fn try_into(self) -> Result<Vec<String>, Self::Error> {
        match self {
            Self::Multiple(vec) => Ok(vec),
            Self::Single(str) => Ok(vec![str]),
        }
    }
}

#[derive(Clone, Debug)]
pub enum TequilaError {
    ActionForbidden,
    ConnectionRefused,
    ProtocolError,
    BadSessionKey,
    ParserBadFormat,
    ParserMissingFields(Vec<String>),
}

#[derive(Clone, Debug)]
pub enum TequilaMethod {
    CreateRequest,
    FetchAttributes,
}

impl TequilaMethod {
    pub fn as_str(&self) -> &'static str {
        match self {
            TequilaMethod::CreateRequest => "createrequest",
            TequilaMethod::FetchAttributes => "fetchattributes",
        }
    }
}
