#[macro_use]
extern crate rocket;

use sqlx::{Pool, Postgres};
use rocket_okapi::{openapi_get_routes, rapidoc::*, settings::UrlObject};

mod env;
mod models;
mod routes;
mod tequila;
mod utils;

#[rocket::main]
async fn main() {
    env::check();

    let database_url = env::DATABASE_URL.clone();
    let pool = sqlx::PgPool::connect(&database_url).await.unwrap();

    apply_migrations(&pool).await;

    let _ = rocket::build()
        .manage(pool)
        .mount("/", routes![index])
        .mount("/api", openapi_get_routes![
            routes::health_check,
            routes::get_attributes,
            routes::services::post_groups,
            routes::services::get_groups,
            routes::tequila::post_tequila_request,
            routes::tequila::post_tequila_fetch_attributes,
        ])
        .mount(
            "/api/rapidoc/",
            make_rapidoc(&RapiDocConfig {
                general: GeneralConfig {
                    spec_urls: vec![UrlObject::new("General", "../openapi.json")],
                    ..Default::default()
                },
                hide_show: HideShowConfig {
                    allow_spec_url_load: false,
                    allow_spec_file_load: false,
                    ..Default::default()
                },
                ..Default::default()
            }),
        )
        .launch().await.expect("Rocket failed to launch");
}

#[get("/")]
fn index() -> rocket::response::content::RawHtml<&'static str> {
    rocket::response::content::RawHtml(r#"
        <div>
            <h1>Welcome to Mojito</h1>
            <h4>This is the temporairy Tequila wrapper for AGEPoly until <a href="https://gitlab.com/agepoly/it/dev/whiskey-api">Whiskey</a> is ready !</h4>
            <br />
            <p>For more information: contact AGEPoly's IT Manager at <a href="mailto:informatique@agepoly.ch">informatique@agepoly.ch</a></p>
        </div>
    "#)
}

async fn apply_migrations(pool: &Pool<Postgres>) {
    let migrator = sqlx::migrate::Migrator::new(std::path::Path::new("./migrations"))
        .await
        .expect("Failed to init migrator");
    let migrations = migrator.migrations.iter();
    println!("Found migrations:");
    for migration in migrations {
        match migration.migration_type {
            sqlx::migrate::MigrationType::ReversibleDown => {}
            _ => println!("  - {} # {}", migration.description, migration.version),
        }
    }
    migrator.run(pool).await.expect("Failed to run migrations");
    println!("Migrations applied");
}
