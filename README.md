# Mojito
Tequila Wrapper for AGEPoly while [Whiskey](https://gitlab.com/agepoly/it/dev/whiskey-api) is still under development.

## How to use it

### Preparations

1. Get the list of all attributes you can use : `curl https://mojito.agepoly.ch/api/service-attributes`
2. Ask AGEPoly's IT Manager for the creation of a new service with
    - your attributes,
    - a return URL for your frontend. Ex: `https://mywebsite.com/callback` will be called like this : `https://mywebsite.com/callback?key=5pl9akpjw5pqjlaz3cexxpd6z8ol6jea&auth_check=75bb616e27e6872c6bd472a1721aaa4b7915829042cb2b3e91ba22f5c762ebfc`
    - an appropriate name for your service
3. You will receive upon approval the following informations:
    - the id of your service (number)
    - the secret token of your service (UUID)

### Implementation in your app

(for specifics : refer to the [API documentation](https://mojito.agepoly.ch/api/rapidoc))

4. Your backend calls `POST https://mojito.agepoly.ch/api/tequila/request` with your id and secret token in JSON
5. Mojito returns the URL the client must be redirected to
6. After your client authenticates, Tequila will redirect to your return URL with the parameters `key` and `auth_check`.
7. Your backend calls `POST https://mojito.agepoly.ch/api/tequila/fetch-attributes` with your id, secret token, tequila key and auth check parameters in JSON
8. Mojito returns the attributes in [the specified format](https://mojito.agepoly.ch/api/rapidoc/index.html#post-/tequila/fetch-attributes)

## Development

Don't forget to prepare SQLx for the CI with `cargo sqlx prepare --database-url postgres://username:password@hostname:5432/database`