CREATE EXTENSION IF NOT EXISTS pgcrypto
    WITH SCHEMA public
    VERSION '1.3';

CREATE TABLE IF NOT EXISTS services (
	id 					SERIAL,
	name 				TEXT NOT NULL,
	secret_token 		UUID NOT NULL DEFAULT gen_random_uuid(),
	activated 			BOOLEAN NOT NULL DEFAULT true,
	tequila_attributes 	TEXT NOT NULL,
	return_url		 	TEXT NOT NULL,
    
	CONSTRAINT tickets_pk PRIMARY KEY (id),
	CONSTRAINT code UNIQUE (id)
);
